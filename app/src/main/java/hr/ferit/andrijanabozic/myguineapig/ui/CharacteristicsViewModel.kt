package hr.ferit.andrijanabozic.myguineapig.ui

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await


suspend fun getNameDataFromFireStore(): List<CharacteristicsScreenState> {
    val dbLocal = FirebaseManager.db
    val characteristicsScreenStateList = mutableListOf<CharacteristicsScreenState>()
    try {
        val querySnapshot = dbLocal.collection("characteristicsName").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(CharacteristicsScreenState::class.java)
            if (result != null) {
                characteristicsScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return characteristicsScreenStateList
}


suspend fun getSexDataFromFireStore(): List<CharacteristicsScreenState> {
    val dbLocal = FirebaseManager.db
    val characteristicsScreenStateList = mutableListOf<CharacteristicsScreenState>()
    try {
        val querySnapshot = dbLocal.collection("characteristicsSex").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(CharacteristicsScreenState::class.java)
            if (result != null) {
                characteristicsScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return characteristicsScreenStateList
}
suspend fun getAgeDataFromFireStore(): List<CharacteristicsScreenState> {
    val dbLocal = FirebaseManager.db
    val characteristicsScreenStateList = mutableListOf<CharacteristicsScreenState>()
    try {
        val querySnapshot = dbLocal.collection("characteristicsAge").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(CharacteristicsScreenState::class.java)
            if (result != null) {
                characteristicsScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return characteristicsScreenStateList
}
suspend fun getWeightDataFromFireStore(): List<CharacteristicsScreenState> {
    val dbLocal = FirebaseManager.db
    val characteristicsScreenStateList = mutableListOf<CharacteristicsScreenState>()
    try {
        val querySnapshot = dbLocal.collection("characteristicsWeight").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(CharacteristicsScreenState::class.java)
            if (result != null) {
                characteristicsScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return characteristicsScreenStateList
}


suspend fun storeCharacteristicsDataInFireStore(
    value: Any?,
    fieldName: String,
    collectionName: String
) {
    val dbLocal = FirebaseManager.db

    try {
        val characteristicData = hashMapOf<String, Any?>(
            fieldName to value
        )

        val docRef = dbLocal.collection(collectionName).document()
        docRef.set(characteristicData).await()
    } catch (e: FirebaseFirestoreException) {
    }
}

suspend fun findNameDocumentIDByData(nameEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("characteristicsName")

    val querySnapshot = collectionReference
        .whereEqualTo("nameEntered", nameEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteNameDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("characteristicsName").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findSexDocumentIDByData(sexEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("characteristicsSex")

    val querySnapshot = collectionReference
        .whereEqualTo("sexEntered", sexEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteSexDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("characteristicsSex").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}


suspend fun findAgeDocumentIDByData(ageEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("characteristicsAge")

    val querySnapshot = collectionReference
        .whereEqualTo("ageEntered", ageEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteAgeDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("characteristicsAge").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findWeightDocumentIDByData(weightEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("characteristicsWeight")

    val querySnapshot = collectionReference
        .whereEqualTo("weightEntered", weightEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteWeightDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("characteristicsWeight").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}




class CharacteristicsViewModel:ViewModel() {
    val state = mutableStateListOf<CharacteristicsScreenState>()

    init {
        getData()
    }
    private fun getData(){
        viewModelScope.launch {
            state.addAll(getNameDataFromFireStore())
            state.addAll(getSexDataFromFireStore())
            state.addAll(getAgeDataFromFireStore())
            state.addAll(getWeightDataFromFireStore())
        }
    }

    fun storeData(value: Any?, fieldName: String, collectionName: String) {
        viewModelScope.launch {
            storeCharacteristicsDataInFireStore(value, fieldName, collectionName)
        }
    }

    fun deleteName(nameEntered: String) {
        viewModelScope.launch {
            val nameId = findNameDocumentIDByData(nameEntered)
            nameId?.let { id ->
                deleteNameDocument(id)
                state.removeAll { it.nameEntered == nameEntered }
            }
        }
    }
    fun deleteSex(sexEntered: String) {
        viewModelScope.launch {
            val sexId = findSexDocumentIDByData(sexEntered)
            sexId?.let { id ->
                deleteSexDocument(id)
                state.removeAll { it.sexEntered == sexEntered }
            }
        }
    }
    fun deleteAge(ageEntered: String) {
        viewModelScope.launch {
            val ageId = findAgeDocumentIDByData(ageEntered)
            ageId?.let { id ->
                deleteAgeDocument(id)
                state.removeAll { it.ageEntered == ageEntered }
            }
        }
    }
    fun deleteWeight(weightEntered: String) {
        viewModelScope.launch {
            val weightId = findWeightDocumentIDByData(weightEntered)
            weightId?.let { id ->
                deleteWeightDocument(id)
                state.removeAll { it.weightEntered == weightEntered }
            }
        }
    }
}

data class CharacteristicsScreenState(
    var nameEntered: String = "",
    var sexEntered: String = "",
    var ageEntered: String = "",
    var weightEntered:String = ""
)

