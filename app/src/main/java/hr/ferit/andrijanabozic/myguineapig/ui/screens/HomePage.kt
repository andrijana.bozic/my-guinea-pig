package hr.ferit.andrijanabozic.myguineapig.ui.screens

import hr.ferit.andrijanabozic.myguineapig.R


import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import hr.ferit.andrijanabozic.myguineapig.ui.Menu
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.TopBar


@Composable
fun HomePage(
    navController: NavHostController,
    guineaPig2: Painter = painterResource(R.drawable.guinea_pig2),
    pear: Painter = painterResource(R.drawable.pear),
    exercise: Painter = painterResource(R.drawable.exercise),
    bathub: Painter = painterResource(R.drawable.bathtub),
    veterinarian: Painter = painterResource(R.drawable.veterinarian),
    history: Painter = painterResource(R.drawable.activity_history)
) {
    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ) {
        Column {
            SideImages()
            Spacer(Modifier.height(30.dp))
            Text(
                text = "My Guinea Pig",
                style = TextStyle(color = Color(0xFF298B28), fontSize = 36.sp,
                    fontWeight = FontWeight.ExtraBold),
                modifier = Modifier.padding(start = 70.dp)
            )
            Text(
                text = "Diary for My Guinea Pig",
                style = TextStyle(color = Color.Black, fontSize = 26.sp,
                    fontWeight = FontWeight.Bold),
                modifier = Modifier.padding(start = 50.dp)
            )
            Spacer(Modifier.height(70.dp))

            Menu("Characteristics", guineaPig2, "Guinea pig", navController,
                Routes.CHARACTERISTICS)
            Menu("Food", pear, "Pear", navController, Routes.FOOD)
            Menu("Physical activity", exercise, "Physical activity", navController,
                Routes.PHYSICAL_ACTIVITY)
            Menu("Hygienic care", bathub, "Hygienic care", navController,
                Routes.HYGIENIC_CARE)
            Menu("Veterinary examination", veterinarian, "Vet", navController,
                Routes.VETERINARY_EXAMINATION)
            Menu("History", history, "History", navController, Routes.HISTORY)

            Spacer(Modifier.height(65.dp))

            SideImages()
        }

    }

}


@Preview
@Composable
fun HomePagePreview() {
}














