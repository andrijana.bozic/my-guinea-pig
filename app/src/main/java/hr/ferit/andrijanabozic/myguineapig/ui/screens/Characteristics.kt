package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.CharacteristicsViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.TopBar


@Composable
fun Characteristics(
    characteristicsViewModel: CharacteristicsViewModel,
    navController: NavHostController,
    guineaPig2: Painter = painterResource(R.drawable.guinea_pig2)

) {
    var nameEntered by remember { mutableStateOf("") }
    var sexEntered by remember { mutableStateOf("") }
    var ageEntered by remember { mutableStateOf("") }
    var weightEntered by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ) {
        Column {
            SideImages()
            Spacer(Modifier.height(30.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Spacer(modifier = Modifier.padding(start = 60.dp))
                TopBar("Characteristics", guineaPig2, "Guinea pig")
            }
            Spacer(Modifier.height(30.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "NAME",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = nameEntered,
                        onValueChange = { nameEntered = it },
                        label = { Text("Enter name") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (nameEntered.isNotEmpty()) {
                                characteristicsViewModel.storeData(nameEntered, "nameEntered", "characteristicsName")
                                nameEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                }
            }

            Spacer(Modifier.height(20.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "SEX",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = sexEntered,
                        onValueChange = { sexEntered = it },
                        label = { Text("Enter sex") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (sexEntered.isNotEmpty()) {
                                characteristicsViewModel.storeData(sexEntered, "sexEntered", "characteristicsSex")
                                sexEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                }
            }

            Spacer(Modifier.height(20.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "AGE",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = ageEntered,
                        onValueChange = { ageEntered = it },
                        label = { Text("Enter age") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (ageEntered.isNotEmpty()) {
                                characteristicsViewModel.storeData(ageEntered, "ageEntered","characteristicsAge")
                                ageEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )

                    }
                }
            }

            Spacer(Modifier.height(20.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "WEIGHT",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = weightEntered,
                        onValueChange = { weightEntered = it },
                        label = { Text("Enter weight in g") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (weightEntered.isNotEmpty()) {
                                characteristicsViewModel.storeData(weightEntered, "weightEntered","characteristicsWeight")
                                weightEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )

                    }
                }
            }


            Spacer(Modifier.height(20.dp))



            Row {
                ButtonNavigation(
                    "BACK",
                    navController,
                    Routes.HOME_PAGE,
                    25.sp,
                    Color(0xFFD2B48C)
                )
                Spacer(Modifier.width(170.dp))
                ButtonNavigation("NEXT", navController, Routes.FOOD, 25.sp, Color(0xFFD2B48C))
            }
            Spacer(Modifier.height(55.dp))

            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation(
                        "HOME",
                        navController,
                        Routes.HOME_PAGE,
                        35.sp,
                        Color(0xFF8B4513)
                    )
                }
            }
        }
    }
}



