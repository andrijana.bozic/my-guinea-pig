package hr.ferit.andrijanabozic.myguineapig.ui

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

suspend fun getFoodDataFromFireStore(): List<FoodScreenState> {
    val dbLocal = FirebaseManager.db
    val foodScreenStateList = mutableListOf<FoodScreenState>()
    try {
        val querySnapshot = dbLocal.collection("food").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(FoodScreenState::class.java)
            if (result != null) {
                foodScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return foodScreenStateList
}

suspend fun storeFoodDataInFireStore(moreFood: List<FoodScreenState>) {
    val dbLocal = FirebaseManager.db
    val batch = dbLocal.batch()

    for (food in moreFood) {
        val foodData = hashMapOf<String, Any?>(
            "hoursOutsideCageEntered" to food.numberOfMealsEntered
        )

        val docRef = dbLocal.collection("food").document()
        batch.set(docRef, foodData)
    }

    try {
        batch.commit().await()
    } catch (e: FirebaseFirestoreException) {
    }
}

suspend fun deleteFoodDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("food").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findFoodDocumentIDByData(numberOfMealsEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("food")

    val querySnapshot = collectionReference
        .whereEqualTo("numberOfMealsEntered", numberOfMealsEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

class FoodViewModel: ViewModel() {
    val state = mutableStateListOf<FoodScreenState>()

    init {
        getData()
    }
    private fun getData(){
        viewModelScope.launch {
            state.addAll(getFoodDataFromFireStore())
        }
    }

    fun storeData(numberOfMealsEntered: String) {
        val state = mutableListOf<FoodScreenState>()
        state.add(FoodScreenState(numberOfMealsEntered))
        viewModelScope.launch {
            storeFoodDataInFireStore(state)
        }
    }

    fun deleteFood(numberOfMealsEntered: String) {
        viewModelScope.launch {
            val nameId = findFoodDocumentIDByData(numberOfMealsEntered)
            nameId?.let { id ->
                deleteFoodDocument(id)
                state.removeAll { it.numberOfMealsEntered == numberOfMealsEntered }
            }
        }
    }
}

data class FoodScreenState(
    var numberOfMealsEntered: String = ""
)

