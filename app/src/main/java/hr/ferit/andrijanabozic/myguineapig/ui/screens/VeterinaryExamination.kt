package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.VetViewModel

@Composable
fun VeterinaryExamination(
    navController: NavHostController,
    vetViewModel: VetViewModel,
) {
    var lastVetExaminationEntered by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ){
        Column{
            SideImages()
            Spacer(Modifier.height(30.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ){
                Column(
                ){
                    Text(
                        text = "Veterinary",
                        style = TextStyle(color = Color.Black, fontSize = 30.sp,
                            fontWeight = FontWeight.Bold),
                        modifier = Modifier.padding(start = 90.dp)
                    )
                    Text(
                        text = "examination",
                        style = TextStyle(color = Color.Black, fontSize = 30.sp,
                            fontWeight = FontWeight.Bold),
                        modifier = Modifier.padding(start = 90.dp)
                    )

                }
                Column{
                    Image(
                        painter = painterResource(R.drawable.veterinarian),
                        contentDescription = "Vet",
                        modifier = Modifier
                            .size(60.dp)
                    )
                }
            }

            Spacer(Modifier.height(30.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "LAST VET EXAMINATION",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = lastVetExaminationEntered,
                        onValueChange = { lastVetExaminationEntered = it },
                        label = { Text("Enter e.g. 14.02.2024") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (lastVetExaminationEntered.isNotEmpty()) {
                                vetViewModel.storeData(lastVetExaminationEntered)
                                lastVetExaminationEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                }
            }

            Spacer(Modifier.height(350.dp))

            Row {
                ButtonNavigation("BACK", navController, Routes.HYGIENIC_CARE,
                    25.sp, Color(0xFFD2B48C))
                Spacer(Modifier.width(170.dp))
                ButtonNavigation("NEXT",navController, Routes.HISTORY,
                    25.sp, Color(0xFFD2B48C))
            }
            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))
                }
            }        
        }
    }
}


