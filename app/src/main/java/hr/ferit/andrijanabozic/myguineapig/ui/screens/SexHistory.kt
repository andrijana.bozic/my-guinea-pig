package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.CharacteristicsViewModel

@Composable
fun SexHistory(navController: NavHostController, characteristicsViewModel: CharacteristicsViewModel) {
    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    )
    {
        Column {

            Spacer(Modifier.height(30.dp))

            Row(
                modifier = Modifier
                    .padding(start = 120.dp)
            ){
                ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))

            }
            Row {
                ButtonNavigation(
                    "BACK", navController, Routes.CHARACTERISTICS_HISTORY,
                    25.sp, Color(0xFFD2B48C)
                )
            }
            val characteristics = characteristicsViewModel.state
            val reversedCharacteristics = characteristics.reversed()

            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
            ) {
                items(reversedCharacteristics) { sex ->
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = sex.sexEntered,
                            style = TextStyle(
                                color = Color.Black,
                                fontSize = 25.sp,
                                fontWeight = FontWeight.Normal
                            ),
                            modifier = Modifier
                                .weight(1f)
                                .padding(start = 30.dp)
                        )
                        if (sex.sexEntered.isNotEmpty()) {
                            Spacer(modifier = Modifier.width(8.dp))
                            Button(
                                onClick = { characteristicsViewModel.deleteSex(sex.sexEntered) },
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(0xFF8B4513),
                                )
                            ) {
                                Text(
                                    "Delete",
                                    fontSize = 18.sp,
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}