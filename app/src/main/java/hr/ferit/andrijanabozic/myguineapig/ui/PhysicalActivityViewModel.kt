package hr.ferit.andrijanabozic.myguineapig.ui

import android.content.ContentValues.TAG
import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

suspend fun getActivityDataFromFireStore(): List<PhysicalActivityScreenState> {
    val dbLocal = FirebaseManager.db
    val physicalActivityScreenStateList = mutableListOf<PhysicalActivityScreenState>()
    try {
        val querySnapshot = dbLocal.collection("physicalActivity").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(PhysicalActivityScreenState::class.java)
            if (result != null) {
                physicalActivityScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return physicalActivityScreenStateList
}
suspend fun storeActivityDataInFireStore(activities: List<PhysicalActivityScreenState>) {
    val dbLocal = FirebaseManager.db
    val batch = dbLocal.batch()

    for (activity in activities) {
        val activityData = hashMapOf<String, Any?>(
            "hoursOutsideCageEntered" to activity.hoursOutsideCageEntered,
            "minutesOutsideCageEntered" to activity.minutesOutsideCageEntered
        )

        val docRef = dbLocal.collection("physicalActivity").document()
        batch.set(docRef, activityData)
    }

    try {
        batch.commit().await()
        Log.d(TAG, "All city data stored successfully.")
    } catch (e: FirebaseFirestoreException) {
        Log.w(TAG, "Error storing city data: $e")
    }
}
suspend fun findActivityDocumentIDByData(hoursOutsideCageEntered: String, minutesOutsideCageEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("physicalActivity")

    val querySnapshot = collectionReference
        .whereEqualTo("hoursOutsideCageEntered", hoursOutsideCageEntered)
        .whereEqualTo("minutesOutsideCageEntered", minutesOutsideCageEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteActivityDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("physicalActivity").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}



class PhysicalActivityViewModel: ViewModel() {
    val state = mutableStateListOf<PhysicalActivityScreenState>()

    init {
        getData()
    }
    private fun getData(){
        viewModelScope.launch {
            state.addAll(getActivityDataFromFireStore())
        }
    }
    fun storeData(hoursOutsideCageEntered: String, minutesOutsideCageEntered: String) {
        val state = mutableListOf<PhysicalActivityScreenState>()
        state.add(PhysicalActivityScreenState(hoursOutsideCageEntered, minutesOutsideCageEntered))
        viewModelScope.launch {
            storeActivityDataInFireStore(state)
        }
    }
    fun deleteDocument(hoursOutsideCageEntered: String, minutesOutsideCageEntered: String) {
        viewModelScope.launch {
            val documentId = findActivityDocumentIDByData(hoursOutsideCageEntered, minutesOutsideCageEntered)
            documentId?.let { id ->
                deleteActivityDocument(id)
                state.removeAll { activity ->
                    activity.hoursOutsideCageEntered == hoursOutsideCageEntered &&
                            activity.minutesOutsideCageEntered == minutesOutsideCageEntered
                }
            }
        }
    }

}
data class PhysicalActivityScreenState(
 var hoursOutsideCageEntered: String = "",
 var minutesOutsideCageEntered: String = ""
)

