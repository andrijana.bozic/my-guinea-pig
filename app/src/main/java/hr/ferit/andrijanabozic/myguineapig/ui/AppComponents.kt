
package hr.ferit.andrijanabozic.myguineapig.ui


import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R


@Composable
fun SideImages() {
    Row {
        Image(
            painter = painterResource(R.drawable.guinea_pig),
            contentDescription = "Guinea pig",
            modifier = Modifier.size(80.dp)
        )
        Spacer(Modifier.width(230.dp))
        Image(
            painter = painterResource(R.drawable.guinea_pig),
            contentDescription = "Guinea pig",
            modifier = Modifier.size(80.dp)
        )
    }
}

@Composable
fun Menu(textValue: String, image: Painter, description: String,
         navController: NavHostController, destination: String
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clickable {
                navController.navigate(destination)
            }

    ){
        Text(
            text = textValue,
            style = TextStyle(color = Color.Black, fontSize = 26.sp,
                fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(start = 30.dp)
        )
        Image(
            painter = image,
            contentDescription = description,
            modifier = Modifier
                .size(60.dp)
        )

    }

}


@Composable
fun TopBar(textValue: String,image: Painter, description: String) {

    Text(text = textValue,
        style = TextStyle(color = Color.Black,
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold),
    )
    Image(
        painter = image,
        contentDescription = description,
        modifier = Modifier
            .size(60.dp)
    )
}


@Composable
fun ButtonNavigation(textValue: String,
                     navController: NavHostController,
                     destination: String, fontSize:TextUnit, containerColor:Color) {
    Button(
        colors = ButtonDefaults.buttonColors(
            containerColor = containerColor,
        ),
        onClick = {
            navController.navigate(destination)
        } ,
    ) {
        Text(
            text = textValue,
            style = TextStyle(color = Color.Black, fontSize = fontSize,
                fontWeight = FontWeight.Bold),
        )
    }
}


@Composable
fun PhysicalActivityScreen(
    physicalActivityViewModel: PhysicalActivityViewModel = viewModel()
) {
    val physicalActivities = physicalActivityViewModel.state

    val reversedActivities = physicalActivities.reversed()

    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
    ) {
        items(reversedActivities) { activity ->
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {

                Text(text = activity.hoursOutsideCageEntered,  style = TextStyle(
                    color = Color.Black,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.Normal
                ),
                    modifier = Modifier
                        .padding(end = 7.dp),
                )
                Text(text = activity.minutesOutsideCageEntered,  style = TextStyle(
                    color = Color.Black,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.Normal
                ),
                    modifier = Modifier
                )
                if (activity.hoursOutsideCageEntered.isNotEmpty() && activity.minutesOutsideCageEntered.isNotEmpty()) {
                    Spacer(modifier = Modifier.width(8.dp))
                    Button(
                        onClick = {
                            physicalActivityViewModel.deleteDocument(activity.hoursOutsideCageEntered, activity.minutesOutsideCageEntered)
                        },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFF8B4513),
                        )
                    ) {
                        Text("Delete")
                    }
                }
            }
        }
    }
}



