package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.TopBar
import androidx.compose.runtime.*
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import hr.ferit.andrijanabozic.myguineapig.ui.HygienicCareViewModel

@Composable
fun HygienicCare(
    navController: NavHostController,
    hygienicCareViewModel: HygienicCareViewModel,
    bathub: Painter = painterResource(R.drawable.bathtub)
) {
    var lastBathEntered by remember { mutableStateOf("") }
    var lastNailsCuttingEntered by remember { mutableStateOf("") }
    var lastCombingEntered by remember { mutableStateOf("") }
    var lastCageWashingEntered by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ){
        Column{
            SideImages()
            Spacer(Modifier.height(30.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ){
                Spacer(modifier = Modifier.padding(start = 70.dp))
                TopBar("Hygienic care", bathub, "Hygienic care")
            }
            Spacer(Modifier.height(30.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "LAST BATH",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = lastBathEntered,
                        onValueChange = { lastBathEntered = it },
                        label = { Text("Enter e.g. 14.02.2024.") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (lastBathEntered.isNotEmpty()) {
                                hygienicCareViewModel.storeData(lastBathEntered, "lastBathEntered", "LastBath")
                                lastBathEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                }
            }
            Spacer(Modifier.height(20.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "LAST NAILS CUTTING",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = lastNailsCuttingEntered,
                        onValueChange = { lastNailsCuttingEntered = it },
                        label = { Text("Enter e.g. 14.02.2024.") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (lastNailsCuttingEntered.isNotEmpty()) {
                                hygienicCareViewModel.storeData(lastNailsCuttingEntered, "lastNailsCuttingEntered", "LastNailsCutting")
                                lastNailsCuttingEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                }
            }

            Spacer(Modifier.height(20.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "LAST COMBING",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = lastCombingEntered,
                        onValueChange = { lastCombingEntered = it },
                        label = { Text("Enter e.g. 14.02.2024") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (lastCombingEntered.isNotEmpty()) {
                                hygienicCareViewModel.storeData(lastCombingEntered, "lastCombingEntered","LastCombing")
                                lastCombingEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )

                    }
                }
            }

            Spacer(Modifier.height(20.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = "LAST CAGE WASHING",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = lastCageWashingEntered,
                        onValueChange = { lastCageWashingEntered = it },
                        label = { Text("Enter e.g. 14.02.2024") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),

                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2B48C),
                        ),
                        onClick = {
                            if (lastCageWashingEntered.isNotEmpty()) {
                                hygienicCareViewModel.storeData(lastCageWashingEntered, "lastCageWashingEntered","LastCageWashing")
                                lastCageWashingEntered = ""
                            } else {
                            }
                        },
                    )
                    {
                        Text(
                            text = "Save",
                            style = TextStyle(
                                color = Color.Black, fontSize = 25.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )

                    }
                }
            }

            Spacer(Modifier.height(70.dp))

            Row {
                ButtonNavigation("BACK", navController, Routes.PHYSICAL_ACTIVITY,
                    25.sp, Color(0xFFD2B48C))
                Spacer(Modifier.width(170.dp))
                ButtonNavigation("NEXT",navController, Routes.VETERINARY_EXAMINATION,
                    25.sp, Color(0xFFD2B48C))
            }
            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))
                }
            }
        }
    }
}

