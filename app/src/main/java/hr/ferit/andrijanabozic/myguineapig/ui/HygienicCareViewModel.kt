package hr.ferit.andrijanabozic.myguineapig.ui

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

suspend fun getLastBathFromFireStore(): List<HygienicCareScreenState> {
    val dbLocal = FirebaseManager.db
    val hygienicCareScreenStateList = mutableListOf<HygienicCareScreenState>()
    try {
        val querySnapshot = dbLocal.collection("LastBath").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(HygienicCareScreenState::class.java)
            if (result != null) {
                hygienicCareScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return hygienicCareScreenStateList
}


suspend fun getLastNailsCuttingFromFireStore(): List<HygienicCareScreenState> {
    val dbLocal = FirebaseManager.db
    val hygienicCareScreenStateList = mutableListOf<HygienicCareScreenState>()
    try {
        val querySnapshot = dbLocal.collection("LastNailsCutting").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(HygienicCareScreenState::class.java)
            if (result != null) {
                hygienicCareScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return hygienicCareScreenStateList
}
suspend fun getLastCombingFromFireStore(): List<HygienicCareScreenState> {
    val dbLocal = FirebaseManager.db
    val hygienicCareScreenStateList = mutableListOf<HygienicCareScreenState>()
    try {
        val querySnapshot = dbLocal.collection("LastCombing").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(HygienicCareScreenState::class.java)
            if (result != null) {
                hygienicCareScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return hygienicCareScreenStateList
}
suspend fun getLastCageWashingFromFireStore(): List<HygienicCareScreenState> {
    val dbLocal = FirebaseManager.db
    val hygienicCareScreenStateList = mutableListOf<HygienicCareScreenState>()
    try {
        val querySnapshot = dbLocal.collection("LastCageWashing").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(HygienicCareScreenState::class.java)
            if (result != null) {
                hygienicCareScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return hygienicCareScreenStateList
}


suspend fun storeHygienicCareDataInFireStore(
    value: Any?,
    fieldName: String,
    collectionName: String
) {
    val dbLocal = FirebaseManager.db

    try {
        val hygienicData = hashMapOf<String, Any?>(
            fieldName to value
        )

        val docRef = dbLocal.collection(collectionName).document()
        docRef.set(hygienicData).await()
    } catch (e: FirebaseFirestoreException) {
    }
}

suspend fun findLastBathDocumentIDByData(lastBathEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("LastBath")

    val querySnapshot = collectionReference
        .whereEqualTo("lastBathEntered", lastBathEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteLastBathDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("LastBath").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findLastNailsCuttingDocumentIDByData(lastNailsCuttingEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("LastNailsCutting")

    val querySnapshot = collectionReference
        .whereEqualTo("lastNailsCuttingEntered", lastNailsCuttingEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteLastNailsCuttingDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("LastNailsCutting").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}


suspend fun findLastCombingDocumentIDByData(lastCombingEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("LastCombing")

    val querySnapshot = collectionReference
        .whereEqualTo("lastCombingEntered", lastCombingEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteLastCombingDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("LastCombing").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findLastCageWashingDocumentIDByData(lastCageWashingEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("LastCageWashing")

    val querySnapshot = collectionReference
        .whereEqualTo("lastCageWashingEntered", lastCageWashingEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

suspend fun deleteLastCageWashingDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("LastCageWashing").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}


class HygienicCareViewModel: ViewModel() {
    val state = mutableStateListOf<HygienicCareScreenState>()

    init {
        getData()
    }

    private fun getData() {
        viewModelScope.launch {
            state.addAll(getLastBathFromFireStore())
            state.addAll(getLastNailsCuttingFromFireStore())
            state.addAll(getLastCombingFromFireStore())
            state.addAll(getLastCageWashingFromFireStore())
        }
    }

    fun storeData(value: Any?, fieldName: String, collectionName: String) {
        viewModelScope.launch {
            storeHygienicCareDataInFireStore(value, fieldName, collectionName)
        }
    }

    fun deleteLastBath(lastBathEntered: String) {
        viewModelScope.launch {
            val nameId = findLastBathDocumentIDByData(lastBathEntered)
            nameId?.let { id ->
                deleteLastBathDocument(id)
                state.removeAll { it.lastBathEntered == lastBathEntered }
            }
        }
    }

    fun deleteLastNailsCutting(lastNailsCuttingEntered: String) {
        viewModelScope.launch {
            val sexId = findLastNailsCuttingDocumentIDByData(lastNailsCuttingEntered)
            sexId?.let { id ->
                deleteLastNailsCuttingDocument(id)
                state.removeAll { it.lastNailsCuttingEntered == lastNailsCuttingEntered }
            }
        }
    }

    fun deleteLastCombing(lastCombingEntered: String) {
        viewModelScope.launch {
            val ageId = findLastCombingDocumentIDByData(lastCombingEntered)
            ageId?.let { id ->
                deleteLastCombingDocument(id)
                state.removeAll { it.lastCombingEntered == lastCombingEntered }
            }
        }
    }

    fun deleteLastCageWashing(lastCageWashingEntered: String) {
        viewModelScope.launch {
            val weightId = findLastCageWashingDocumentIDByData(lastCageWashingEntered)
            weightId?.let { id ->
                deleteLastCageWashingDocument(id)
                state.removeAll { it.lastCageWashingEntered == lastCageWashingEntered }
            }
        }
    }
}

data class HygienicCareScreenState(
    var lastBathEntered: String = "",
    var lastNailsCuttingEntered: String = "",
    var lastCombingEntered: String = "",
    var lastCageWashingEntered:String = ""
)
