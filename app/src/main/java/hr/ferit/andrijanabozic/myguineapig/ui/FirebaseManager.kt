package hr.ferit.andrijanabozic.myguineapig.ui

import com.google.firebase.firestore.FirebaseFirestore

object FirebaseManager {
    val db = FirebaseFirestore.getInstance()
}
