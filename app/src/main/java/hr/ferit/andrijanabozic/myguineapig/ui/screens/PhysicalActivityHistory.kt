package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.PhysicalActivityScreen

@Composable
fun PhysicalActivityHistory(navController: NavHostController) {
    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    )
    {
        Column {
            Spacer(Modifier.height(30.dp))

            Row(
                modifier = Modifier
                    .padding(start = 120.dp)
            ){
                ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))

            }
            Row {
                ButtonNavigation(
                    "BACK", navController, Routes.HISTORY,
                    25.sp, Color(0xFFD2B48C)
                )
            }
            PhysicalActivityScreen()
            Spacer(Modifier.height(30.dp))
        }
    }
}

