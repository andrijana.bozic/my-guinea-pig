package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.Menu
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages

@Composable
fun HygienicCareHistory(navController: NavHostController,
                        history: Painter = painterResource(R.drawable.activity_history)
) {
    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    )
    {
        Column(){
            SideImages()

            Spacer(Modifier.height(80.dp))

            Menu("Bath History", history, "History", navController,
                Routes.LAST_BATH_HISTORY)
            Spacer(Modifier.height(30.dp))

            Menu("Nails Cutting History", history, "History", navController, Routes.LAST_NAILS_CUTTING_HISTORY)
            Spacer(Modifier.height(30.dp))

            Menu("Combing History", history, "History", navController, Routes.LAST_COMBING_HISTORY)
            Spacer(Modifier.height(30.dp))

            Menu("Cage Washing History", history, "History", navController,
                Routes.LAST_CAGE_WASHING_HISTORY)

            Spacer(Modifier.height(140.dp))

            Row {
                ButtonNavigation(
                    "BACK", navController, Routes.HISTORY,
                    25.sp, Color(0xFFD2B48C)
                )
            }
            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))
                }
            }
        }
    }
}


