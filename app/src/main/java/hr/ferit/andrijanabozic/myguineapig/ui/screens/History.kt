package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.Menu
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.TopBar

@Composable
fun History(
    navController: NavHostController,
    history: Painter = painterResource(R.drawable.activity_history)
) {

    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ){
        Column{
            SideImages()
            Spacer(Modifier.height(30.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ){
                Spacer(modifier = Modifier.padding(start = 115.dp))
                TopBar("History", history, "History")
            }

            Spacer(Modifier.height(30.dp))

            Menu("Characteristics", history, "History", navController,
                Routes.CHARACTERISTICS_HISTORY)
            Menu("Food", history, "History", navController, Routes.FOOD_HISTORY)
            Menu("Physical activity", history, "History", navController,
                Routes.PHYSICAL_ACTIVITY_HISTORY)
            Menu("Hygienic care", history, "History", navController,
                Routes.HYGIENIC_CARE_HISTORY)
            Menu("Veterinary examination", history, "History", navController,
                Routes.VETERINARY_EXAMINATION_HISTORY)

            Spacer(Modifier.height(130.dp))

            Row {
                ButtonNavigation("BACK", navController, Routes.VETERINARY_EXAMINATION,
                    25.sp, Color(0xFFD2B48C))
                Spacer(Modifier.width(170.dp))
                ButtonNavigation(
                    "NEXT", navController, Routes.PHYSICAL_ACTIVITY,
                    25.sp, Color(0xFFD2B48C)
                )
            }

            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))
                }
            }
        }
    }
}
