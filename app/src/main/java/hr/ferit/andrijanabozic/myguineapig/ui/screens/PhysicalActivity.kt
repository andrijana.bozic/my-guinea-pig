package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import hr.ferit.andrijanabozic.myguineapig.R
import hr.ferit.andrijanabozic.myguineapig.ui.ButtonNavigation
import hr.ferit.andrijanabozic.myguineapig.ui.PhysicalActivityViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.SideImages
import hr.ferit.andrijanabozic.myguineapig.ui.TopBar
import androidx.compose.runtime.*
import androidx.compose.ui.draw.clip

@Composable
fun PhysicalActivity(
    physicalActivityViewModel: PhysicalActivityViewModel,
    navController: NavHostController,
    exercise: Painter = painterResource(R.drawable.exercise)
) {
    var hoursOutsideCage by remember { mutableStateOf("") }
    var minutesOutsideCage by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .background(Color(0xFFD2B48C))
            .fillMaxSize()
    ){
        Column{
           SideImages()
            Spacer(Modifier.height(30.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ){
                Spacer(modifier = Modifier.padding(start = 60.dp))
                TopBar("Physical activity", exercise, "Physical activity")
            }
            Text(
                text = "Daily",
                style = TextStyle(color = Color.Black, fontSize = 26.sp,
                    fontWeight = FontWeight.Bold),
                modifier = Modifier.padding(start = 150.dp)
            )
            Spacer(Modifier.height(30.dp))

            Row{
                Column {
                    Text(
                        text = "HOURS OUTSIDE THE CAGE",
                        style = TextStyle(
                            color = Color.Black, fontSize = 15.sp,
                            fontWeight = FontWeight.Normal
                        ),
                        modifier = Modifier.padding(start = 30.dp)
                    )
                    TextField(
                        value = hoursOutsideCage,
                        onValueChange = { hoursOutsideCage = it },
                        label = { Text("Enter hours outside the cage") },
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .width(250.dp)
                            .clip(RoundedCornerShape(16.dp)),
                        textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                    )
                }
            }
            Spacer(Modifier.height(20.dp))

            Row{
               Column {
                   Text(
                       text = "MINUTES OUTSIDE THE CAGE",
                       style = TextStyle(
                           color = Color.Black, fontSize = 15.sp,
                           fontWeight = FontWeight.Normal
                       ),
                       modifier = Modifier.padding(start = 30.dp)
                   )
                   TextField(
                       value = minutesOutsideCage,
                       onValueChange = { minutesOutsideCage = it },
                       label = { Text("Enter minutes outside Cage") },
                       modifier = Modifier
                           .padding(start = 20.dp)
                           .width(250.dp)
                           .clip(RoundedCornerShape(16.dp)),
                       textStyle = TextStyle.Default.copy(fontSize = 20.sp),
                   )
               }
           }
            Row(
                modifier = Modifier.padding(start = 80.dp)
            ){
                Button(
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFFD2B48C),
                    ),
                    onClick = {
                        if(hoursOutsideCage.isNotEmpty() && minutesOutsideCage.isNotEmpty()){
                            physicalActivityViewModel.storeData(hoursOutsideCage, minutesOutsideCage)
                            hoursOutsideCage = ""
                            minutesOutsideCage = ""
                        }else{

                        }
                    } ,
                    modifier = Modifier
                )
                {

                    Text(
                        text = "Save",
                        style = TextStyle(color = Color.Black, fontSize = 25.sp,
                            fontWeight = FontWeight.Bold),
                    )
                }
            }

            Spacer(Modifier.height(180.dp))

            Row {
                ButtonNavigation("BACK", navController, Routes.FOOD,
                    25.sp, Color(0xFFD2B48C))
                Spacer(Modifier.width(170.dp))
                ButtonNavigation("NEXT",navController, Routes.HYGIENIC_CARE,
                    25.sp, Color(0xFFD2B48C))
            }
            Box {
                SideImages()
                Row(
                    modifier = Modifier
                        .padding(start = 120.dp)
                ) {
                    ButtonNavigation("HOME", navController, Routes.HOME_PAGE, 35.sp, Color(0xFF8B4513))
                }
            }
        }
    }
}

