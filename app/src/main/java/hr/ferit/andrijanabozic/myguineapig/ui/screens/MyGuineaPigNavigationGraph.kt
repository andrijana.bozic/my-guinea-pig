package hr.ferit.andrijanabozic.myguineapig.ui.screens

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import hr.ferit.andrijanabozic.myguineapig.ui.CharacteristicsViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.FoodViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.HygienicCareViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.PhysicalActivityViewModel
import hr.ferit.andrijanabozic.myguineapig.ui.VetViewModel


@Composable
fun MyGuineaPigNavigationGraph(
    characteristicsViewModel: CharacteristicsViewModel = viewModel(),
    foodViewModel: FoodViewModel = viewModel(),
    physicalActivityViewModel: PhysicalActivityViewModel = viewModel(),
    hygienicCareViewModel: HygienicCareViewModel = viewModel(),
    vetViewModel: VetViewModel = viewModel(),)
{
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Routes.HOME_PAGE ){
        composable(Routes.HOME_PAGE){
            HomePage(navController)
        }
        composable(Routes.CHARACTERISTICS){
            Characteristics(characteristicsViewModel, navController)
        }
        composable(Routes.FOOD){
            Food(characteristicsViewModel, navController)
        }
        composable(Routes.PHYSICAL_ACTIVITY){
            PhysicalActivity(physicalActivityViewModel, navController)
        }
        composable(Routes.HYGIENIC_CARE){
            HygienicCare(navController, hygienicCareViewModel)
        }
        composable(Routes.VETERINARY_EXAMINATION){
            VeterinaryExamination(navController, vetViewModel)
        }
        composable(Routes.HISTORY){
            History(navController)
        }
        composable(Routes.CHARACTERISTICS_HISTORY){
            CharacteristicsHistory(navController)
        }
        composable(Routes.FOOD_HISTORY){
            FoodHistory(navController, foodViewModel)
        }
        composable(Routes.PHYSICAL_ACTIVITY_HISTORY){
            PhysicalActivityHistory(navController)
        }
        composable(Routes.HYGIENIC_CARE_HISTORY){
            HygienicCareHistory(navController)
        }
        composable(Routes.VETERINARY_EXAMINATION_HISTORY){
            VeterinaryExaminationHistory(navController, vetViewModel)
        }
        composable(Routes.NAME_HISTORY){
            NameHistory(navController, characteristicsViewModel)
        }
        composable(Routes.SEX_HISTORY){
            SexHistory(navController, characteristicsViewModel)
        }
        composable(Routes.AGE_HISTORY){
            AgeHistory(navController, characteristicsViewModel)
        }
        composable(Routes.WEIGHT_HISTORY){
            WeightHistory(navController, characteristicsViewModel)
        }
        composable(Routes.LAST_BATH_HISTORY){
            LastBathHistory(navController, hygienicCareViewModel)
        }
        composable(Routes.LAST_NAILS_CUTTING_HISTORY){
            LastNailsCuttingHistory(navController, hygienicCareViewModel)
        }
        composable(Routes.LAST_COMBING_HISTORY){
            LastCombingHistory(navController, hygienicCareViewModel)
        }
        composable(Routes.LAST_CAGE_WASHING_HISTORY){
            LastCageWashingHistory(navController, hygienicCareViewModel)
        }
    }
}