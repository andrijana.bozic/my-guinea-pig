package hr.ferit.andrijanabozic.myguineapig.ui

import android.content.ContentValues.TAG
import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

suspend fun getVetDataFromFireStore(): List<VetScreenState> {
    val dbLocal = FirebaseManager.db
    val vetScreenStateList = mutableListOf<VetScreenState>()
    try {
        val querySnapshot = dbLocal.collection("vet").get().await()
        for (document in querySnapshot.documents) {
            val result = document.toObject(VetScreenState::class.java)
            if (result != null) {
                vetScreenStateList.add(result)
            }
        }
    } catch (e: FirebaseFirestoreException) {
        Log.d("error", "getDataFromFireStore: $e")
    }
    return vetScreenStateList
}

suspend fun storeVetDataInFireStore(moreVetDate: List<VetScreenState>) {
    val dbLocal = FirebaseManager.db
    val batch = dbLocal.batch()

    for (vetDate in moreVetDate) {
        val vetData = hashMapOf<String, Any?>(
            "lastVetExaminationEntered" to vetDate.lastVetExaminationEntered
        )

        val docRef = dbLocal.collection("vet").document()
        batch.set(docRef, vetData)
    }

    try {
        batch.commit().await()
        Log.d(TAG, "All city data stored successfully.")
    } catch (e: FirebaseFirestoreException) {
        Log.w(TAG, "Error storing city data: $e")
    }
}

suspend fun deleteVetDocument(documentId: String) {
    try {
        val dbLocal = FirebaseManager.db
        val documentReference = dbLocal.collection("vet").document(documentId)
        documentReference.delete().await()
        Log.d("FirestoreRepository", "Document $documentId successfully deleted.")
    } catch (e: Exception) {
        Log.e("FirestoreRepository", "Error deleting document: $e")
    }
}

suspend fun findVetDocumentIDByData(lastVetExaminationEntered: String): String? {
    val dbLocal = FirebaseManager.db
    val collectionReference = dbLocal.collection("vet")

    val querySnapshot = collectionReference
        .whereEqualTo("lastVetExaminationEntered", lastVetExaminationEntered)
        .get()
        .await()

    if (!querySnapshot.isEmpty) {
        return querySnapshot.documents.first().id
    }

    return null
}

class VetViewModel: ViewModel() {
    val state = mutableStateListOf<VetScreenState>()

    init {
        getData()
    }
    private fun getData(){
        viewModelScope.launch {
            state.addAll(getVetDataFromFireStore())
        }
    }

    fun storeData(lastVetExaminationEntered: String) {
        val state = mutableListOf<VetScreenState>()
        state.add(VetScreenState(lastVetExaminationEntered))
        viewModelScope.launch {
            storeVetDataInFireStore(state)
        }
    }

    fun deleteVetData(lastVetExaminationEntered: String) {
        viewModelScope.launch {
            val vetId = findVetDocumentIDByData(lastVetExaminationEntered)
            vetId?.let { id ->
                deleteVetDocument(id)
                state.removeAll { it.lastVetExaminationEntered == lastVetExaminationEntered }
            }
        }
    }
}

data class VetScreenState(
    var lastVetExaminationEntered: String = ""
)

